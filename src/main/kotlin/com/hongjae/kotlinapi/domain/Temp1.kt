package com.hongjae.kotlinapi.domain

import javax.persistence.*

@Entity
@Table(name = "temp1")
data class Temp1(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var Id: Long?,
    var data1: String,
    var data2: String
)
